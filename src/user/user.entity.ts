import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsEmail, IsNotEmpty, Length } from 'class-validator';

@Entity('users')
export default class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Length(3, 50, { message: 'Name should be between 3-50' })
  name: string;

  @Column()
  @Length(6, 15, { message: 'Password should be between 6-15 letters long' })
  password: string;

  @Column()
  @IsNotEmpty({ message: 'Email is requried' })
  @IsEmail({}, { message: 'Please enter a valid email' })
  email: string;
}
